<!DOCTYPE doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="favicon.ico" rel="icon">
    <title>
        Pencil Solutions
    </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/all.css" rel="stylesheet">
    <link href="css/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/aos.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

</head>

<body>

    <!-- <div class="loading-page">
        <div class="spinner">
            <div class="sk-wave">
                <div class="sk-rect sk-rect1"></div>
                <div class="sk-rect sk-rect2"></div>
                <div class="sk-rect sk-rect3"></div>
                <div class="sk-rect sk-rect4"></div>
                <div class="sk-rect sk-rect5"></div>
            </div>
        </div>
    </div> -->

    <header class="pt-5">
        <div class="container">
            <a href="index.php">
                <img src="images/logo.png" />
            </a>
            <button class="btn btn-primary float-right h-65 lh-50" data-target="#bookYourTeam" data-toggle="modal">
                Start Now
            </button>
            <div class="col-md-8">
                <div class="caption">
                    <h1 data-aos="fade-up" data-aos-duration="1000" class="pt-5 mt-5">
                        Hire a
                        <span class="primary-underlined">
                        </span> agile software team to develop your project
                        <span class="green-underlined">
                        </span>
                    </h1>
                    <p data-aos="fade-up" data-aos-duration="1500">
                        Simply tell us about your project, the technical resources you want to book and instantly get
                        your
                        project's monthly expenses and a team ready to start from today.
                    </p>
                    <a data-aos="fade-up" data-aos-duration="2000" class="btn btn-primary mt-3 h-65 lh-50"
                        data-target="#bookYourTeam" data-toggle="modal" href="#">
                        Book your team
                    </a>
                </div>
            </div>
        </div>
    </header>
    <main>
        <section class="pt-5 pb-5 mb-5">
            <div class="container">
                <div data-aos="fade-up" data-aos-duration="500">
                    <h1>
                        Our Services
                    </h1>
                    <p>
                        We provide fully-managed end-to-end software development teams to work on your web
                        <br> and mobile software products starting from product design all the way to go-live

                    </p>
                </div>
                <div class="row mt-5">
                    <div class="col col-md-4 col-sm-6 col-12" data-aos="fade-up" data-aos-duration="500">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="images/s1.png">
                            <div class="card-body">
                                <h1 class="card-title">
                                    Custom Web Development
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_1.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_2.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col col-md-4 col-sm-6 col-12" data-aos="fade-up" data-aos-duration="750">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="images/s2.png">
                            <div class="card-body">
                                <h1 class="card-title">
                                    Mobile apps
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_4.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_5.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col col-md-4 col-sm-6 col-12" data-aos="fade-up" data-aos-duration="1000">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="images/s3.png">
                            <div class="card-body">
                                <h1 class="card-title">
                                    E-Commerce
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_6.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_7.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="why">
            <div class="container">
                <div data-aos="fade-up" data-aos-duration="500">
                    <h1>
                        Why Choose Us?
                    </h1>
                    <p class="text-muted">
                        Why we might be the perfect fit for your needs
                    </p>
                </div>
                <div class="row mt-5">
                    <div class="blocks mb-4">
                        <div class="d-flex align-items-start" data-aos="fade-up" data-aos-duration="1000">
                            <img class="mr-3" src="images/ic_whyus_1.svg">
                            <div>
                                <h4>
                                    Your project development starts today
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start" data-aos="fade-up" data-aos-duration="1000">
                            <img class="mr-3" src="images/ic_whyus_2.svg">
                            <div>
                                <h4>
                                    Transparent Communication with a dedicated Account Manager
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start" data-aos="fade-up" data-aos-duration="1000">
                            <img class="mr-3" src="images/ic_whyus_3.svg">
                            <div>
                                <h4>
                                    Most competitive rates in the world!
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start" data-aos="fade-up" data-aos-duration="1000">
                            <img class="mr-3" src="images/ic_whyus_4.svg">
                            <div>
                                <h4>
                                    All support resources will be included in the quotation as needed
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start" data-aos="fade-up" data-aos-duration="1000">
                            <img class="mr-3" src="images/ic_whyus_5.svg">
                            <div>
                                <h4>
                                    Switch your team into support mode and reduce your expenses towards the end of the
                                    project
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough,
                                    we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <button class="btn btn-primary h-65 lh-50" data-aos="fade-up" data-aos-duration="1500"
                                data-target="#bookYourTeam" data-toggle="modal">
                                Book your team
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 mt-5 pb-5" id="featured-work">
            <div class="container">
                <h1>
                    Featured Work
                </h1>
                <p class="text-muted">
                    Why we might be the perfect fir for your needs
                </p>
                <div class="row">
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_1.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    Optio
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_2.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    CPMS
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_3.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    Seasoned
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_1.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    Optio
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_2.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    CPMS
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col" data-aos="fade-up" data-aos-duration="500">
                        <div class="team-box">
                            <a href="details.php" class="team-img">
                                <img alt="images" src="images/work_4.jpg" />
                            </a>
                            <h4>
                                <a href="details.php">
                                    Seasoned
                                </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="col-12 text-center" data-aos="fade-up" data-aos-duration="1000">
                        <a class="btn btn-primary mt-3 h-65 lh-50" href="#">
                            More
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 mt-5 pb-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4" data-aos="fade-up" data-aos-duration="1000">
                        <h1>
                            Our Team
                        </h1>
                        <p>
                            We provide fully-managed end-to-end software development teams to work on your web and
                            mobile software products starting from product design all the way to go-live
                        </p>
                    </div>
                    <div class="col-md-8" data-aos="fade-up" data-aos-duration="1000">
                        <img class="img-fluid" src="images/team.jpg">

                    </div>
                </div>
            </div>
        </section>
        <section class="contact-us pb-5" id="contact-us">
            <div class="container">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-lg-7 col-12 md-bottom-0" data-aos="fade-up" data-aos-duration="1000">
                            <h1 class="text-white pt-5 font-weight-normal">
                            Drop us a line
                            </h1>
                            <small class="text-white">
                                Send us anything, from what you think to what you want
                            </small>
                            <form class="contact-form pt-4">
                                <div class="row">
                                    <div class="col-md-8 col-12">
                                        <label class="control-label">Your Name</label>
                                        <input placeholder="ex. John Smith.." type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-12">
                                    <label class="control-label">Email Address</label>
                                        <input placeholder="ex. potato@gmail.com" type="email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-12">
                                        <label class="control-label">Message</label>
                                        <textarea placeholder="Hi, you have nice designs.."></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input class="fill-btn yellow-btn" type="button" value="Send Message" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-5 col-12 md-bottom-0" data-aos="fade-up" data-aos-duration="1000">

                            <div class="contact-container">
                                <h1 class="font-weight-normal">
                                    Reach us at
                                </h1>
                                <small>
                                    Find, call, meet us at
                                </small>
                                <div class="contact-form pt-4">
                                    <ul class="details">
                                    <li>
                                            <p>
                                                <i class="fas fa-envelope"></i>
                                                info@pencil-designs.com
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                            <i class="fas fa-globe-africa"></i>
                                                <a class="text-warning" href="http://www.pencil-designs.com">
                                                    www.pencil-designs.com
                                                </a>
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                <i class="fas fa-mobile-alt">
                                                </i> +20 012 234 5678
                                            </p>
                                        </li>
                                        
                                        <li>
                                            <p>
                                                <i class="fas fa-map-marker-alt">
                                                </i> Sheraton, Helipolis, Cairo, Egypt
                                            </p>
                                        </li>
                                        
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <footer class="pt-5 pb-4" id="footer">
        <div class="container">
            <div class="wrapper">
                <div class="pad-both-15 pad-top-22">

                    <div class="row justify-content-between">
                        <div class="footer-right col-lg-5 col-12">
                            <a class="footer-logo" href="#">
                                <img alt="logo" src="images/logo.png" />
                            </a>
                            <p>
                                We’re Pencil. A young but powerful team of developers and designers with one objective in mind. Making the web more friendlier.
                            </p>
                        </div>
                        <div class="footer-left">
                            <ul>
                                <li><a href="#">About Pencil</a></li>
                                <li><a href="#">Projects</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Work</a></li>
                                <li><a href="#">Service</a></li>
                                <li><a href="#">Company</a></li>
                            </ul>
                        </div>

                    </div>

                        <div class="copyright d-flex justify-content-between">
                            <h6>
                                © 2018
                                <a href="#">
                                    Pencil
                                </a> Terms & Conditions
                            </h6>
                            <div class="footer-social-icons float-right">
                                <ul class="flat-social-icons">
                                    <li><a href="#" class="social-icon facebook"> <i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#" class="social-icon twitter"> <i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#" class="social-icon instagram"> <i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>   
                        </div>
                  
                </div>
            </div>
        </div>
    </footer>
    <a class="back-to-top" href="javascript:void(0);">
        <i aria-hidden="true" class="fa flaticon-arrows">
        </i>
    </a>
    <div class="sendSuccess" id="sendSuccess">
    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" data-backdrop="static"
        data-keyboard="false" id="bookYourTeam" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document" style="max-width:1200px">
            <div class="modal-content">
            <div class="container">
            
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><img src="images/logo_icon_white.svg" alt="" /></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="images/ic_close.svg" alt="" />
                    </button>
                </div>
                <div class="modal-body">
                    
                    <form id="wizard" method="POST">   
                        <h1>
                            1
                        </h1>
                        <div class="step-content">
                            <div class="m-t-md">
                                <h2 class="text-center mb-5">
                                    Tell Us About Who You Need
                                </h2>
                                <div class="step-container">

                                    
                                    <div class="row">
                                        <div class="col-md-8 col-12">
                                            <div class="row text-center">

                                                <div class="col-sm-3 col-6">
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="200" id="ux-designer"
                                                            type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="ux-designer">
                                                            UX
                                                            Designer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="500" id="ui-designer"
                                                            type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="ui-designer">
                                                            UI
                                                            Designer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="3000"
                                                            id="graphic-designer" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="graphic-designer">
                                                            Graphic
                                                            Designer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="2000"
                                                            id="brand-director" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="brand-director">
                                                            Brand
                                                            Director
                                                        </label>

                                                    </div>

                                                </div>
                                                <div class="col-sm-3 col-6">
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="5000"
                                                            id="frontend-dev" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="frontend-dev">
                                                            Frontend
                                                            Developer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="5000"
                                                            id="backend-dev" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="backend-dev">
                                                            Backend
                                                            Developer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="3000" id="ui-dev"
                                                            type="checkbox">
                                                        <label class="custom-control-label" for="ui-dev">
                                                            UI
                                                            Developer
                                                        </label>

                                                    </div>
                                                    
                                                </div>
                                                <div class="col-sm-3 col-6">
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="6000"
                                                            id="android-dev" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="android-dev">
                                                            Android
                                                            Developer
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="8000" id="ios-dev"
                                                            type="checkbox">
                                                        <label class="custom-control-label" for="ios-dev">
                                                            IOS
                                                            Developer
                                                        </label>
                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="10000"
                                                            id="product-manager" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="product-manager">
                                                            Product
                                                            Manager
                                                        </label>

                                                    </div>
                                                   
                                                    
                                                </div>

                                                <div class="col-sm-3 col-6">
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="10000"
                                                            id="art-director" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="art-director">
                                                            Art Director
                                                        </label>

                                                    </div>
                                                    <div
                                                        class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                        <input class="custom-control-input" data-cost="9000"
                                                            id="project-manager" type="checkbox">
                                                        <label class="custom-control-label"
                                                            for="project-manager">
                                                            Project
                                                            Manager
                                                        </label>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="right-panel">
                                                <p class="select-stmnt">
                                                    Select your team members to continue...
                                                </p>
                                                <div class="selected-team-area">
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="cost-area mb-4">
                                                    <h5 class="text-muted text-center">
                                                        Monthly Cost
                                                    </h5>
                                                    <div class="cost-area-inner">
                                                    </div>

                                                    <a href="#" class="continue continue-btn btn-arrow btn btn-primary">
                                                        <span>Continue <i class="fas fa-arrow-right"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1>
                            2
                        </h1>
                        <div class="step-content">
                            <div class="m-t-md">
                                <h2 class="text-center">
                                    Tell us about your project
                                </h2>
                                <div class="step-container mt-50">
                                    <a href="#" class="prev-btn"><img src="images/ic_back.svg" alt="" /></a>
                                    <div class="row">

                                        <div class="col-md-6 offset-md-3 col-12 schedule-meeting">
                                            <div class="form-group">
                                                <label>
                                                Project Overview <sup>*</sup>
                                                <span class="overview-msg error-msg">Please enter project overview.</span>
                                                </label>
                                                <textarea class="form-control" rows="5" id="overview" name="overview" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                 Your Name <sup>*</sup>
                                                 <span class="name-msg error-msg">Please enter your name.</span>
                                                </label>
                                                <input class="form-control" id="name" name="name" type="text" required />
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                Your Email <sup>*</sup>
                                                <span class="email-msg error-msg">Please enter your email.</span>
                                                <span class="valid-msg error-msg">Please enter a valid email.</span>
                                                </label>
                                                <input class="form-control" id="email" name="email" type="text" required />
                                            </div>
                                            <!-- <fieldset class="form-group">
                                                <legend class="col-form-label pt-0">
                                                    Do you have a team manager?
                                                </legend>
                                                <div>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input autocomplete="off" checked="" id="option4" name="team"
                                                                type="radio" value="Yes">
                                                            Yes

                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" id="option5" name="team" type="radio"
                                                                value="No">
                                                            No

                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <legend class="col-form-label pt-0">
                                                    Will you need product design support (UX/UI)?
                                                </legend>
                                                <div>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" checked="" id="option1" name="support"
                                                                type="radio" value="Yes">
                                                            Yes
                                                        </label>
                                                        <label class="btn btn-secondary active">
                                                            <input autocomplete="off" id="option2" name="support"
                                                                type="radio" value="No">
                                                            No
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" id="option3" name="support"
                                                                type="radio" value="I don't Know">
                                                            I don't know

                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset> -->
                                            <input type="hidden" class="monthly_cost" name="monthly_cost">
                                            <input type="hidden" class="members" name="members">
                                            <div class="schedule-btn text-left">
                                                <button class="continue-btn btn-arrow submitForm btn btn-primary">Schedule A Meeting</button>
                                                <p class="select-stmnt schedule-data"></p>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4 right-panel">
                                            <div class="cost-area mb-4">
                                                <h5 class="text-muted text-center">
                                                    Monthly Cost
                                                </h5>
                                                <div class="stored-cost">
                                                </div>
                                            </div>
                                            <p class="select-stmnt">
                                                Select your team members to continue...
                                            </p>
                                            <div class="selected-team-area">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1>
                            3
                        </h1>
                        <div class="step-content">
                            <div class="m-t-md">
                                <h2 class="text-center">
                                    Now it’s time to meet
                                </h2>
                                <div class="step-container mt-50">
                                    <a href="#" class="prev-btn"><img src="images/ic_back.svg" alt="" /></a>
                                        <!-- Calendly inline widget begin -->
                                        <div class="calendly-inline-widget" data-url="https://calendly.com/sabry/30min"
                                            style="min-width:320px;height:650px;">
                                        </div>
                                        <script src="https://assets.calendly.com/assets/external/widget.js"
                                            type="text/javascript">
                                        </script>
                                        <!-- Calendly inline widget end -->

                                        <div class="text-center">
                                            <a href="#" class="finish continue-btn btn-arrow btn btn-primary">
                                                Finish
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- <h1>
                            Complete Your Request
                        </h1>
                        <div class="step-content">
                            <div class="text-center m-t-md">
                                <div class="mt-5">
                                    <div class="row">
                                        <div class="modalbox success col-sm-8 col-md-6 col-lg-5 center animate">
                                            <div class="icon">
                                                <span class="fas fa-check">
                                                </span>
                                            </div>
                                            <h1>
                                                Success!
                                            </h1>
                                            <p>
                                                We've sent a confirmation to your e-mail
                                                <br> for verification.

                                            </p>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
                </div>
            </div>
        </div>
    </div>
    <!--Popper for bootstrap dropdown menus (must be included before bootstrap JS)-->


    <script src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Steps -->
    <script src="js/steps/jquery.steps.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/aos.js"></script>
    <script>
        AOS.init({ duration: 2000, });
    </script>
    <script>

        $(document).ready(function () {

            $('[data-target=#bookYourTeam]').click(function () {

                $("#wizard-t-0").click();
                $("[aria-selected='false']").removeClass('done');
                $('[aria-selected="false"]').addClass('disabled');
                $("#wizard .actions a[href='#next']").hide();
                $("#wizard .actions a[href='#previous']").hide();
                $("#wizard .actions a[href='#cancel']").hide();
                $("#wizard .actions a[href='#finish']").hide();

                $(".continue").click(function () {
                    $("#wizard .actions a[href='#next']").click();
                });
                $(".prev-btn").click(function () {
                    $("#wizard .actions a[href='#previous']").click();
                });

                $(".finish").click(function () {
                    $("#wizard .actions a[href='#finish']").click();
                });
            });

            $("#wizard").steps({
                startIndex: 0,
                saveState: false,
                enableCancelButton: true,
                transitionEffect: "fade",
                labels: {
                    cancel: "Cancel",
                    current: "current step:",
                    pagination: "Pagination",
                    finish: "Finish",
                    next: "Continue",
                    previous: "Previous",
                    loading: "Loading ..."
                },
                onCanceled: function () {
                    $("#bookYourTeam").modal('hide');
                },
                onStepChanging: function (event, currentIndex, newIndex) {

                    if (newIndex == 1) {
                        $("#wizard .actions a[href='#next']").hide();
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }

                    return true;
                },
                onStepChanged: function (event, currentIndex, priorIndex) {
                    // console.log({event, currentIndex, priorIndex})
                    if (currentIndex == 2) {
                        $("#wizard .actions a[href='#finish']").hide();
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }
                },
                onFinished: function () {
                    $("#bookYourTeam").modal('hide');
                    location.reload(true);
                }
            });
        });
    </script>

    <script>

        
        $(document).ready(function () {
            $(".submitForm").click(function() {

                var overview = $('#overview').val().trim();
                var name = $('#name').val().trim();
                var email = $('#email').val().trim();

                if(overview== ''){
                    $(".overview-msg").show();
                    return false;
                } else {
                    $(".overview-msg").hide();
                }
                if(name== ''){
                    $(".name-msg").show();
                    return false;
                } else {
                    $(".name-msg").hide();
                }
                if(email== ''){
                    $(".valid-msg").show();
                    return false;
                } else {
                    $(".valid-msg").hide();
                }
                if(IsEmail(email) == false){
                    $(".valid-msg").show();
                    return false;
                } else {
                    $(".valid-msg").hide();
                }

                    var url = "ajax.php";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#wizard").serialize(),
                        success: function (res) {
                            $("#sendSuccess").text(res);
                            $("#sendSuccess").slideDown();
                            setTimeout(function () {
                                $("#sendSuccess").slideUp();
                            }, 3000);
                            $("#wizard .actions a[href='#next']").click();
                            // $("#wizard .actions a[href='#previous']").show();
                            // $("#wizard .actions a[href='#cancel']").show();
                            // $("#wizard .actions a[href='#next']").show();
                        },
                        error: function (req, status, err) {
                            console.log('something went wrong', status, err);
                        }
                    });
                return false;
            });
        });

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)) {
                return false;
            } else{
                return true;
            }
        }

    </script>


    <!-- Checkbox -->
    <script>
        var selectedTeam = [],
            cost = [],
            totalCost = 0,

            stmntText = function () {
                if (selectedTeam.length == 0) {
                    $(".select-stmnt").text("Select your team members to continue...");
                    $(".cost-area").css('display', 'none');

                } else {
                    $(".select-stmnt").html("You currently have <strong>" + selectedTeam.length + '</strong> member(s) in your team');
                    $(".cost-area").css('display', 'block');
                }
            };

        $(document).ready(function () {

            $(".custom-control-label").on('click', function () {

                if ($(this).siblings()[0].checked) {
                    totalCost = 0;

                    $(this).parent().removeClass('selected');

                    $(".selected-team-area").html('');
                    var index = selectedTeam.indexOf($(this).siblings()[0].id);
                    var costIndex = cost.indexOf($(this).siblings().attr('data-cost'));
                    selectedTeam.splice(index, 1);
                    cost.splice(costIndex, 1);
                    stmntText();

                    $(".cost-area-inner").html(function () {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;
                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {
                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);

                } else {

                    $(this).parent().addClass('selected');

                    $(".selected-team-area").html('');
                    totalCost = 0;
                    selectedTeam.push($(this).siblings()[0].id);
                    cost.push($(this).siblings().attr('data-cost'));
                    stmntText();

                    $(".cost-area-inner").html(function () {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;

                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {

                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);
                    $(".monthly_cost").val(totalCost)
                    $(".members").val(selectedTeam)
                }

            });

        });



    </script>

</body>

</html>