$(document).ready(function () {

    $('html, body').css({ 'height': '100%' });
    $(window).load(function () {
        $(".loading-page").fadeOut(3000, function () {
            $('html, body').removeAttr('style');
        });
    });

});

