<!DOCTYPE doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="favicon.ico" rel="icon">
    <title>
        Pencil Solutions
    </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/all.css" rel="stylesheet">
    <link href="css/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/aos.css" rel="stylesheet">
    <link href="css/smoothDivScroll.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

</head>

<body>

    <!-- <div class="loading-page">
        <div class="spinner">
            <div class="sk-wave">
                <div class="sk-rect sk-rect1"></div>
                <div class="sk-rect sk-rect2"></div>
                <div class="sk-rect sk-rect3"></div>
                <div class="sk-rect sk-rect4"></div>
                <div class="sk-rect sk-rect5"></div>
            </div>
        </div>
    </div> -->

    <header class="pt-5 pageHeader">
        <div class="container">
            <a href="index.html">
                <img src="images/logo.png" />
            </a>
            <button class="btn btn-primary float-right h-65 lh-50" data-target="#bookYourTeam" data-toggle="modal">
                Start Now
            </button>
        </div>
    </header>
    <main>
        <section class="details pt-5 pb-5 mb-5">
            <div class="container">

                <div class="details-main">
                    <div class="img-block">
                        <div class="img-container">
                            <img src="images/top.png" class="img-fluid" alt="" />
                        </div>
                    </div>
                    <ul>
                        <li>
                            <label>CLIENT</label>
                            <h4>Seasoned Restaurants</h4>
                        </li>
                        <li>
                            <label>SOLUTIONS</label>
                            <h4>User Experience Design</h4>
                        </li>
                        <li>
                            <label>DATE</label>
                            <h4>November 2019</h4>
                        </li>
                    </ul>
                </div>


                <div class="content-details">

                    <h1 data-aos="fade-up" data-aos-duration="500">
                        Working with Design systems in 2019
                    </h1>

                    <h4 data-aos="fade-up" data-aos-duration="500">New Design Iteration</h4>
                    <p data-aos="fade-up" data-aos-duration="500">
                        It was time to revisit our initial designs and solve some of the issues we had found during the
                        usability testing stages. Based on what we’ve learned so far, we started redesigning several
                        screens and flows.
                    </p>
                    <p data-aos="fade-up" data-aos-duration="500">
                        Firstly, we improved the main screen experience. As we found out earlier, users needed more
                        information to decide if they wanted to watch a recording, so we thought that adding the time of
                        creation indicator and the number of likes and views to the video list items would partially
                        solve this problem.
                    </p>
                </div>

                <div class="cards-container details-main">
                    <div class="img-block card-img" data-aos="fade-up" data-aos-duration="500">
                        <div class="img-container">
                            <img class="img-fluid" src="images/detailone.png" alt="" />
                        </div>
                    </div>
                    <div class="img-block card-img" data-aos="fade-up" data-aos-duration="600">
                        <div class="img-container">
                            <img class="img-fluid" src="images/detailtwo.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="apps">

            <div id="makeMeScrollable">
                <img src="images/mobiles.png" alt="" />
                <img src="images/mobiles.png" alt="" />
                <img src="images/mobiles.png" alt="" />
            </div>

        </div>

        <section class="details pt-5 pb-5">
            <div class="container">

                <div class="details-main mt-2">
                    <div class="img-block">
                        <div class="img-container">
                            <img src="images/bottom.png" data-aos="fade-up" data-aos-duration="500" class="img-fluid" alt="" />
                        </div>
                    </div>
                </div>

                <div class="content-details">

                    <h1 class="mb-4" data-aos="fade-up" data-aos-duration="500">
                        Maximizing Revenues at Pencil
                    </h1>

                    <p data-aos="fade-up" data-aos-duration="500">
                        It was time to revisit our initial designs and solve some of the issues we had found during the
                        usability testing stages. Based on what we’ve learned so far, we started redesigning several
                        screens and flows.
                    </p>
                    <p data-aos="fade-up" data-aos-duration="500">
                        Firstly, we improved the main screen experience. As we found out earlier, users needed more
                        information to decide if they wanted to watch a recording, so we thought that adding the time of
                        creation indicator and the number of likes and views to the video list items would partially
                        solve this problem.
                    </p>
                </div>


                <div class="contact-us start-project">
                    <div class="wrapper">
                        <h1 data-aos="fade-up" data-aos-duration="500">
                            Like this project? start your own project now
                        </h1>
                        <p data-aos="fade-up" data-aos-duration="500">
                            Get a free consultation - moneyback guarantee - no credit card required
                        </p>

                        <button data-aos="fade-up" data-aos-duration="500" data-target="#bookYourTeam" data-toggle="modal"
                            class="continue-btn btn-arrow btn btn-primary">
                            <span>
                                BOOK YOUR TEAM NOW
                                <i class="fas fa-arrow-right"></i>
                            </span>
                        </button>
                    </div>
                </div>


            </div>
        </section>

    </main>

    <footer class="pt-5 pb-4" id="footer">
        <div class="container">
            <div class="wrapper">
                <div class="pad-both-15 pad-top-22">

                    <div class="row justify-content-between">
                        <div class="footer-right col-lg-5 col-12">
                            <a class="footer-logo" href="#">
                                <img alt="logo" src="images/logo.png" />
                            </a>
                            <p>
                                We’re Pencil. A young but powerful team of developers and designers with one objective
                                in mind. Making the web more friendlier.
                            </p>
                        </div>
                        <div class="footer-left">
                            <ul>
                                <li><a href="#">About Pencil</a></li>
                                <li><a href="#">Projects</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Work</a></li>
                                <li><a href="#">Service</a></li>
                                <li><a href="#">Company</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="copyright d-flex justify-content-between">
                        <h6>
                            © 2018
                            <a href="#">
                                Pencil
                            </a> Terms & Conditions
                        </h6>
                        <div class="footer-social-icons float-right">
                            <ul class="flat-social-icons">
                                <li><a href="#" class="social-icon facebook"> <i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="social-icon twitter"> <i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="social-icon instagram"> <i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
    <a class="back-to-top" href="javascript:void(0);">
        <i aria-hidden="true" class="fa flaticon-arrows">
        </i>
    </a>
    <div class="sendSuccess" id="sendSuccess">
    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" data-backdrop="static"
        data-keyboard="false" id="bookYourTeam" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document" style="max-width:1200px">
            <div class="modal-content">
                <div class="container">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><img src="images/logo_icon_white.svg" alt="" />
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="images/ic_close.svg" alt="" />
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="wizard" method="POST">
                            <h1>
                                1
                            </h1>
                            <div class="step-content">
                                <div class="m-t-md">
                                    <h2 class="text-center mb-5">
                                        Tell Us About Who You Need
                                    </h2>
                                    <div class="step-container">


                                        <div class="row">
                                            <div class="col-md-8 col-12">
                                                <div class="row text-center">

                                                    <div class="col-sm-3 col-6">
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="200"
                                                                id="ux-designer" type="checkbox">
                                                            <label class="custom-control-label" for="ux-designer">
                                                                UX
                                                                Designer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="500"
                                                                id="ui-designer" type="checkbox">
                                                            <label class="custom-control-label" for="ui-designer">
                                                                UI
                                                                Designer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="3000"
                                                                id="graphic-designer" type="checkbox">
                                                            <label class="custom-control-label" for="graphic-designer">
                                                                Graphic
                                                                Designer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="2000"
                                                                id="brand-director" type="checkbox">
                                                            <label class="custom-control-label" for="brand-director">
                                                                Brand
                                                                Director
                                                            </label>

                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3 col-6">
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="5000"
                                                                id="frontend-dev" type="checkbox">
                                                            <label class="custom-control-label" for="frontend-dev">
                                                                Frontend
                                                                Developer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="5000"
                                                                id="backend-dev" type="checkbox">
                                                            <label class="custom-control-label" for="backend-dev">
                                                                Backend
                                                                Developer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="3000"
                                                                id="ui-dev" type="checkbox">
                                                            <label class="custom-control-label" for="ui-dev">
                                                                UI
                                                                Developer
                                                            </label>

                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3 col-6">
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="6000"
                                                                id="android-dev" type="checkbox">
                                                            <label class="custom-control-label" for="android-dev">
                                                                Android
                                                                Developer
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="8000"
                                                                id="ios-dev" type="checkbox">
                                                            <label class="custom-control-label" for="ios-dev">
                                                                IOS
                                                                Developer
                                                            </label>
                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="10000"
                                                                id="product-manager" type="checkbox">
                                                            <label class="custom-control-label" for="product-manager">
                                                                Product
                                                                Manager
                                                            </label>

                                                        </div>


                                                    </div>

                                                    <div class="col-sm-3 col-6">
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="10000"
                                                                id="art-director" type="checkbox">
                                                            <label class="custom-control-label" for="art-director">
                                                                Art Director
                                                            </label>

                                                        </div>
                                                        <div
                                                            class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                            <input class="custom-control-input" data-cost="9000"
                                                                id="project-manager" type="checkbox">
                                                            <label class="custom-control-label" for="project-manager">
                                                                Project
                                                                Manager
                                                            </label>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="right-panel">
                                                    <p class="select-stmnt">
                                                        Select your team members to continue...
                                                    </p>
                                                    <div class="selected-team-area">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="cost-area mb-4">
                                                        <h5 class="text-muted text-center">
                                                            Monthly Cost
                                                        </h5>
                                                        <div class="cost-area-inner">
                                                        </div>

                                                        <a href="#"
                                                            class="continue continue-btn btn-arrow btn btn-primary">
                                                            <span>Continue <i class="fas fa-arrow-right"></i></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h1>
                                2
                            </h1>
                            <div class="step-content">
                                <div class="m-t-md">
                                    <h2 class="text-center">
                                        Tell us about your project
                                    </h2>
                                    <div class="step-container mt-50">
                                        <a href="#" class="prev-btn"><img src="images/ic_back.svg" alt="" /></a>
                                        <div class="row">

                                            <div class="col-md-6 offset-md-3 col-12 schedule-meeting">
                                                <div class="form-group">
                                                    <label>
                                                        Project Overview <sup>*</sup>
                                                        <span class="overview-msg error-msg">Please enter project
                                                            overview.</span>
                                                    </label>
                                                    <textarea class="form-control" rows="5" id="overview"
                                                        name="overview" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        Your Name <sup>*</sup>
                                                        <span class="name-msg error-msg">Please enter your name.</span>
                                                    </label>
                                                    <input class="form-control" id="name" name="name" type="text"
                                                        required />
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        Your Email <sup>*</sup>
                                                        <span class="email-msg error-msg">Please enter your
                                                            email.</span>
                                                        <span class="valid-msg error-msg">Please enter a valid
                                                            email.</span>
                                                    </label>
                                                    <input class="form-control" id="email" name="email" type="text"
                                                        required />
                                                </div>
                                                <!-- <fieldset class="form-group">
                                                <legend class="col-form-label pt-0">
                                                    Do you have a team manager?
                                                </legend>
                                                <div>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input autocomplete="off" checked="" id="option4" name="team"
                                                                type="radio" value="Yes">
                                                            Yes

                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" id="option5" name="team" type="radio"
                                                                value="No">
                                                            No

                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <legend class="col-form-label pt-0">
                                                    Will you need product design support (UX/UI)?
                                                </legend>
                                                <div>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" checked="" id="option1" name="support"
                                                                type="radio" value="Yes">
                                                            Yes
                                                        </label>
                                                        <label class="btn btn-secondary active">
                                                            <input autocomplete="off" id="option2" name="support"
                                                                type="radio" value="No">
                                                            No
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input autocomplete="off" id="option3" name="support"
                                                                type="radio" value="I don't Know">
                                                            I don't know

                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset> -->
                                                <input type="hidden" class="monthly_cost" name="monthly_cost">
                                                <input type="hidden" class="members" name="members">
                                                <div class="schedule-btn text-left">
                                                    <button
                                                        class="continue-btn btn-arrow submitForm btn btn-primary">Schedule
                                                        A Meeting</button>
                                                    <p class="select-stmnt schedule-data"></p>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-4 right-panel">
                                            <div class="cost-area mb-4">
                                                <h5 class="text-muted text-center">
                                                    Monthly Cost
                                                </h5>
                                                <div class="stored-cost">
                                                </div>
                                            </div>
                                            <p class="select-stmnt">
                                                Select your team members to continue...
                                            </p>
                                            <div class="selected-team-area">
                                            </div>
                                        </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h1>
                                3
                            </h1>
                            <div class="step-content">
                                <div class="m-t-md">
                                    <h2 class="text-center">
                                        Now it’s time to meet
                                    </h2>
                                    <div class="step-container mt-50">
                                        <a href="#" class="prev-btn"><img src="images/ic_back.svg" alt="" /></a>
                                        <!-- Calendly inline widget begin -->
                                        <div class="calendly-inline-widget" data-url="https://calendly.com/sabry/30min"
                                            style="min-width:320px;height:650px;">
                                        </div>
                                        <script src="https://assets.calendly.com/assets/external/widget.js"
                                            type="text/javascript">
                                            </script>
                                        <!-- Calendly inline widget end -->

                                        <div class="text-center">
                                            <a href="#" class="finish continue-btn btn-arrow btn btn-primary">
                                                Finish
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <h1>
                            Complete Your Request
                        </h1>
                        <div class="step-content">
                            <div class="text-center m-t-md">
                                <div class="mt-5">
                                    <div class="row">
                                        <div class="modalbox success col-sm-8 col-md-6 col-lg-5 center animate">
                                            <div class="icon">
                                                <span class="fas fa-check">
                                                </span>
                                            </div>
                                            <h1>
                                                Success!
                                            </h1>
                                            <p>
                                                We've sent a confirmation to your e-mail
                                                <br> for verification.

                                            </p>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
                </div>
            </div>
        </div>
    </div>
    <!--Popper for bootstrap dropdown menus (must be included before bootstrap JS)-->


    <script src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Steps -->
    <script src="js/steps/jquery.steps.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.mousewheel.min.js"></script>
    <script src="js/jquery.kinetic.js"></script>
    <script src="js/jquery.smoothdivscroll-1.3-min.js"></script>
    <script>
        AOS.init({ duration: 2000, });
    </script>

    <script>

        $(document).ready(function () {

            $('[data-target=#bookYourTeam]').click(function () {

                $("#wizard-t-0").click();
                $("[aria-selected='false']").removeClass('done');
                $('[aria-selected="false"]').addClass('disabled');
                $("#wizard .actions a[href='#next']").hide();
                $("#wizard .actions a[href='#previous']").hide();
                $("#wizard .actions a[href='#cancel']").hide();
                $("#wizard .actions a[href='#finish']").hide();

                $(".continue").click(function () {
                    $("#wizard .actions a[href='#next']").click();
                });
                $(".prev-btn").click(function () {
                    $("#wizard .actions a[href='#previous']").click();
                });

                $(".finish").click(function () {
                    $("#wizard .actions a[href='#finish']").click();
                });
            });

            $("#wizard").steps({
                startIndex: 0,
                saveState: false,
                enableCancelButton: true,
                transitionEffect: "fade",
                labels: {
                    cancel: "Cancel",
                    current: "current step:",
                    pagination: "Pagination",
                    finish: "Finish",
                    next: "Continue",
                    previous: "Previous",
                    loading: "Loading ..."
                },
                onCanceled: function () {
                    $("#bookYourTeam").modal('hide');
                },
                onStepChanging: function (event, currentIndex, newIndex) {

                    if (newIndex == 1) {
                        $("#wizard .actions a[href='#next']").hide();
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }

                    return true;
                },
                onStepChanged: function (event, currentIndex, priorIndex) {
                    // console.log({event, currentIndex, priorIndex})
                    if (currentIndex == 2) {
                        $("#wizard .actions a[href='#finish']").hide();
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }
                },
                onFinished: function () {
                    $("#bookYourTeam").modal('hide');
                    location.reload(true);
                }
            });
        });
    </script>

    <script>


        $(document).ready(function () {
            $(".submitForm").click(function () {

                var overview = $('#overview').val().trim();
                var name = $('#name').val().trim();
                var email = $('#email').val().trim();

                if (overview == '') {
                    $(".overview-msg").show();
                    return false;
                } else {
                    $(".overview-msg").hide();
                }
                if (name == '') {
                    $(".name-msg").show();
                    return false;
                } else {
                    $(".name-msg").hide();
                }
                if (email == '') {
                    $(".valid-msg").show();
                    return false;
                } else {
                    $(".valid-msg").hide();
                }
                if (IsEmail(email) == false) {
                    $(".valid-msg").show();
                    return false;
                } else {
                    $(".valid-msg").hide();
                }

                var url = "ajax.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#wizard").serialize(),
                    success: function (res) {
                        $("#sendSuccess").text(res);
                        $("#sendSuccess").slideDown();
                        setTimeout(function () {
                            $("#sendSuccess").slideUp();
                        }, 3000);
                        $("#wizard .actions a[href='#next']").click();
                        // $("#wizard .actions a[href='#previous']").show();
                        // $("#wizard .actions a[href='#cancel']").show();
                        // $("#wizard .actions a[href='#next']").show();
                    },
                    error: function (req, status, err) {
                        console.log('something went wrong', status, err);
                    }
                });
                return false;
            });
        });

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }

    </script>


    <!-- Checkbox -->
    <script>
        var selectedTeam = [],
            cost = [],
            totalCost = 0,

            stmntText = function () {
                if (selectedTeam.length == 0) {
                    $(".select-stmnt").text("Select your team members to continue...");
                    $(".cost-area").css('display', 'none');

                } else {
                    $(".select-stmnt").html("You currently have <strong>" + selectedTeam.length + '</strong> member(s) in your team');
                    $(".cost-area").css('display', 'block');
                }
            };

        $(document).ready(function () {

            $(".custom-control-label").on('click', function () {

                if ($(this).siblings()[0].checked) {
                    totalCost = 0;

                    $(this).parent().removeClass('selected');

                    $(".selected-team-area").html('');
                    var index = selectedTeam.indexOf($(this).siblings()[0].id);
                    var costIndex = cost.indexOf($(this).siblings().attr('data-cost'));
                    selectedTeam.splice(index, 1);
                    cost.splice(costIndex, 1);
                    stmntText();

                    $(".cost-area-inner").html(function () {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;
                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {
                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);

                } else {

                    $(this).parent().addClass('selected');

                    $(".selected-team-area").html('');
                    totalCost = 0;
                    selectedTeam.push($(this).siblings()[0].id);
                    cost.push($(this).siblings().attr('data-cost'));
                    stmntText();

                    $(".cost-area-inner").html(function () {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;

                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {

                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);
                    $(".monthly_cost").val(totalCost)
                    $(".members").val(selectedTeam)
                }

            });

        });

        // Initialize the plugin with no custom options
        $(document).ready(function () {
            // None of the options are set
            $("div#makeMeScrollable").smoothDivScroll({
                hotSpotScrolling: false,
                touchScrolling: true,
                manualContinuousScrolling: true,
                mousewheelScrolling: false,
                autoScrollingMode: "onStart",
                startAutoScrolling: true

            });
        });

        $("#makeMeScrollable").bind("mouseleave", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        }).bind("mouseup", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        }).bind("mouseout", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        }).bind("mouseover", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        }).bind("mousemove", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        }).bind("mousedown", function () {
            $("#makeMeScrollable").smoothDivScroll("startAutoScrolling");
        });

    </script>

</body>

</html>