function loadMenu() {
    // Close menu in canvas mode
    $('.close-canvas-menu').on('click', function() {
        $("body").toggleClass("mini-navbar");
    });
    // Minimalize menu
    $('.navbar-minimalize').on('click', function() {
        $("body").toggleClass("mini-navbar");
    });
}