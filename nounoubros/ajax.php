<?php

if(!isset($_POST['full_name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['company_name']) ||
        !isset($_POST['mobile']) ||
        !isset($_POST['message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    } else {
 	// EDIT THE 2 LINES BELOW AS REQUIRED
  $recipients = array(
            'ahmed_fci23@yahoo.com'
          );
    $email_to = implode(',', $recipients);
    $email_subject = $_POST['email'] ? $_POST['email'] : 'Contact form';

 
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
   
    $full_name = $_POST['full_name'];
    $email_from = $_POST['email'];
    $telephone = $_POST['mobile'];
    $company_name = $_POST['company_name'];
    $company_name = $_POST['country'];
    $company_name = $_POST['website'];
     
 
    $email_message .= "Name: ".clean_string($full_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "mobile: ".clean_string($mobile)."\n";
    $email_message .= "Company name: ".clean_string($company_name)."\n";
    $email_message .= "Country: ".clean_string($country)."\n";
    $email_message .= "Website: ".clean_string($website)."\n";
    $email_message .= "Message: ".clean_string($_POST['message'])."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  

 
 
$message = 'Thank you for contacting us. We will be in touch with you very soon.';
echo $message;
 die;
 
}

 function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die;
       
    }

     function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
?>