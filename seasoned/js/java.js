$(document).ready(function() {

    $(".image-more").click(function() {
        var image = $(this).attr("src");
        $(".image-view").hide().fadeOut(500);
        $(".image-view").attr("src", image).fadeIn(500);
        return false;
    });

    var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 300,
        easingType: 'linear'
    };
    $().UItoTop({easingType: 'easeOutQuart'});

    $(".boot-tooltip a").tooltip({
        placement: 'top'
    });

    $(".hover").mouseleave(
        function () {
          $(this).removeClass("hover");
        }
      );

});