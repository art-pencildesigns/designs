<!DOCTYPE doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="favicon.ico" rel="icon">
    <title>
        Pencil Solutions
    </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/all.css" rel="stylesheet">
    <link href="css/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <header class="pt-5">
        <div class="container">
            <a href="index.html">
                <img src="images/logo.png" />
            </a>
            <button class="btn btn-primary float-right h-65 lh-50" data-target="#bookYourTeam" data-toggle="modal">
                    Start Now
                </button>
            <div class="col-md-8">
                <br>
                <br>
                <br>
                <br>
                <h1 class="pt-5 mt-5">
                    Hire a
                    <span class="primary-underlined">
                                        </span> agile software team to develop your project
                    <span class="green-underlined">
                                        </span>
                </h1>
                <p>
                    Simply tell us about your project, the technical resources you want to book and instantly get your project's monthly expenses and a team ready to start from today.
                </p>
                <a class="btn btn-primary mt-3 h-65 lh-50" data-target="#bookYourTeam" data-toggle="modal" href="#">
                                        Book
                    your team
                                    </a>

            </div>
        </div>
    </header>
    <main>
        <section class="pt-5 pb-5 mb-5">
            <div class="container">
                <h1>
                    Our Services
                </h1>
                <p>
                    We provide fully-managed end-to-end software development teams to work on your web
                    <br> and mobile software products starting from product design all the way to go-live

                </p>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="http://placehold.it/100x100/">
                            <div class="card-body">
                                <h1 class="card-title">
                                    Custom Web Development
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_1.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_2.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="http://placehold.it/100x100/">
                            <div class="card-body">
                                <h1 class="card-title">
                                    Mobile apps
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_4.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_5.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img alt="Card image cap" class="card-img-top" src="http://placehold.it/100x100/">
                            <div class="card-body">
                                <h1 class="card-title">
                                    E-Commerce
                                </h1>
                                <p class="card-text">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                                <h6 class="font-weight-bold">
                                    Technologies
                                </h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <img src="images/tech_6.png" />
                                    </li>
                                    <li class="list-inline-item">
                                        <img src="images/tech_7.png" />
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="why">
            <div class="container">
                <h1>
                    Why Choose Us?
                </h1>
                <p class="text-muted">
                    Why we might be the perfect fit for your needs
                </p>
                <div class="row m-0">
                    <div class="blocks mb-4">
                        <div class="d-flex align-items-start">
                            <img class="mr-3" src="images/ic_whyus_1@1x.png">
                            <div>
                                <h4>
                                    Your project development starts today
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start">
                            <img class="mr-3" src="images/ic_whyus_2@1x.png">
                            <div>
                                <h4>
                                    Transparent Communication with a dedicated Account Manager
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start">
                            <img class="mr-3" src="images/ic_whyus_3@1x.png">
                            <div>
                                <h4>
                                    Most competitive rates in the world!
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start">
                            <img class="mr-3" src="images/ic_whyus_4@1x.png">
                            <div>
                                <h4>
                                    All support resources will be included in the quotation as needed
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-start">
                            <img class="mr-3" src="images/ic_whyus_5@1x.png">
                            <div>
                                <h4>
                                    Switch your team into support mode and reduce your expenses towards the end of the project
                                </h4>
                                <p class="text-muted">
                                    Motivation is the key to positivity in our lives. If we were not motivated enough, we would just not have the will in us to go about our daily lives.
                                </p>
                            </div>

                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <button class="btn btn-primary h-65 lh-50" data-target="#bookYourTeam" data-toggle="modal">
                                    Book your team
                                </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 mt-5 pb-5" id="featured-work">
            <div class="container">
                <h1>
                    Featured Work
                </h1>
                <p class="text-muted">
                    Why we might be the perfect fir for your needs
                </p>
                <div class="row">
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_1.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        Optio
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_2.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        CPMS
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_3.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        Seasoned
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_1.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        Optio
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_2.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        CPMS
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="item col">
                        <div class="team-box">
                            <div class="team-img">
                                <img alt="images" src="images/work_3.jpg" />
                            </div>
                            <h4>
                                <a href="#">
                                        Seasoned
                                    </a>
                            </h4>
                            <h5>
                                UX/UI Design, Interaction
                            </h5>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <a class="btn btn-primary mt-3 h-65 lh-50" href="#">
                                More
                            </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 mt-5 pb-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <h1>
                            Our Team
                        </h1>
                        <p>
                            We provide fully-managed end-to-end software development teams to work on your web and mobile software products starting from product design all the way to go-live
                        </p>
                    </div>
                    <div class="col-md-8">
                        <img class="img-fluid" src="images/team.jpg">

                    </div>
                </div>
            </div>
        </section>
        <section class="contact-us pb-5" id="contact-us">
            <div class="container">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-8 md-bottom-0">
                            <h1 class="text-white pt-5 font-weight-normal">
                                Contact
                                <span>
                                        Us
                                    </span>
                            </h1>
                            <small class="text-white">
                                    You can contact for any further information
                                </small>
                            <form class="contact-form pt-4">
                                <div class="row">
                                    <div class="col-6 sd-col-12">
                                        <input placeholder="Your Name" type="text" />
                                    </div>
                                    <div class="col-6 sd-col-12">
                                        <input placeholder="Email Address" type="email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input placeholder="Subject" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <textarea placeholder="Message">
                                            </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input class="fill-btn yellow-btn" type="button" value="Send Message" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-4 md-bottom-0">
                            <h1 class="text-white pt-5 font-weight-normal">
                                Contact
                                <span>
                                        Details
                                    </span>
                            </h1>
                            <small class="text-white">
                                    You can contact for any further information
                                </small>
                            <div class="contact-form pt-4">
                                <ul class="container details">
                                    <li>
                                        <p>
                                            <i class="fas fa-mobile-alt mr-2">
                                                </i> +20 012 234 5678
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fas fa-at mr-2">
                                                </i> info@pencil-designs.com
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fas fa-map-marker-alt mr-2">
                                                </i> Sheraton, Helipolis, Cairo, Egypt
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fas fa-globe-africa mr-2">
                                                </i>
                                            <a class="text-warning" href="#">
                                                    www.pencil-designs.com
                                                </a>
                                        </p>
                                    </li>
                                </ul>
                                <div class="or">
                                    Or
                                </div>
                                <button class="btn btn-primary h-65 lh-50 col mt-4" data-target="#bookYourTeam" data-toggle="modal">
                                Book your team now
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


    <footer class="pt-5 pb-5" id="footer">
        <div class="container">
            <div class="wrapper">
                <div class="row pad-both-15 pad-top-22">
                    <div class="col-5 md-col-12 foot-left">
                        <div class="copyright">
                            <a class="footer-logo" href="#">
                                <img alt="logo" src="images/logo.png" />
                            </a>
                            <h6>
                                ©
                                <a href="#">
                                Pencil
                            </a> 2018
                            </h6>
                        </div>
                    </div>
                    <div class="col-7 md-col-12 foot-right md-bottom-0">
                        <div class="social-icons float-right">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/pencildesignsstudio" target="_blank">
                                        <i class="fab fa-facebook-f">
                                    </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fab fa-twitter">
                                    </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/pencil-designs/" target="_blank">
                                        <i class="fab fa-linkedin-in">
                                    </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/pencildesignsstudio/" target="_blank">
                                        <i class="fab fa-instagram">
                                    </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.behance.net/pencildesigns" target="_blank">
                                        <i class="fab fa-behance">
                                    </i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a class="back-to-top" href="javascript:void(0);">
        <i aria-hidden="true" class="fa flaticon-arrows">
    </i>
    </a>
    <div class="sendSuccess" id="sendSuccess">
    </div>
    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" data-backdrop="static" data-keyboard="false" id="bookYourTeam" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document" style="max-width:1200px">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <div class="modal-body">
                    <form id="wizard" method="POST">
                        <h1>
                            Your Team
                        </h1>
                        <div class="step-content">
                            <div class="m-t-md">
                                <h2 class="text-center mb-5">
                                    Tell Us About Who You Need
                                </h2>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row text-center">
                                            <div class="col-4">
                                                <h3>
                                                    Design
                                                </h3>
                                                <hr>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="200" id="ux-designer" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="ux-designer">
                                                            UX
                                                        Designer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="500" id="ui-designer" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="ui-designer">
                                                            UI
                                                        Designer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="3000" id="graphic-designer" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="graphic-designer">
                                                            Graphic
                                                        Designer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="2000" id="brand-director" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="brand-director">
                                                            Brand
                                                        Director
                                                        </label>

                                                </div>

                                            </div>
                                            <div class="col-4">
                                                <h3>
                                                    Development
                                                </h3>
                                                <hr>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="5000" id="frontend-dev" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="frontend-dev">
                                                            Frontend
                                                        Developer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="5000" id="backend-dev" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="backend-dev">
                                                            Backend
                                                        Developer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="3000" id="ui-dev" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="ui-dev">
                                                            UI
                                                        Developer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="8000" id="ios-dev" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="ios-dev">
                                                            IOS
                                                        Developer
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="6000" id="android-dev" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="android-dev">
                                                            Android
                                                        Developer
                                                        </label>

                                                </div>

                                            </div>
                                            <div class="col-4">
                                                <h3>
                                                    Management
                                                </h3>
                                                <hr>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="10000" id="product-manager" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="product-manager">
                                                            Product
                                                        Manager
                                                        </label>

                                                </div>
                                                <div class="form-group custom-control custom-checkbox mb-4 custom-chkbx">
                                                    <input class="custom-control-input" data-cost="9000" id="project-manager" type="checkbox">
                                                    <label class="custom-control-label font-weight-bold" for="project-manager">
                                                            Project
                                                        Manager
                                                        </label>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 right-panel">
                                        <div class="cost-area mb-4">
                                            <h5 class="text-muted text-center">
                                                Monthly Cost
                                            </h5>
                                            <div class="cost-area-inner">
                                            </div>
                                        </div>
                                        <p class="select-stmnt">
                                            Select your team members to continue...
                                        </p>
                                        <div class="selected-team-area">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <h1>
                            Your Project
                        </h1>
                        <div class="step-content">
                            <div class="m-t-md">
                                <h2 class="text-center">
                                    Tell us about your project
                                </h2>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>
                                            Project Overview
                                        </label>
                                            <textarea class="form-control" name="overview"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                            Your Name
                                        </label>
                                            <input class="form-control" name="name" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                            Your Email
                                        </label>
                                            <input class="form-control" name="email" type="text" />
                                        </div>
                                        <fieldset class="form-group">
                                            <legend class="col-form-label pt-0">
                                                Do you have a team manager?
                                            </legend>
                                            <div>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary active">
                                                    <input autocomplete="off" checked="" id="option4" name="team" type="radio" value="Yes">
                                                        Yes
                                                
                                                </label>
                                                    <label class="btn btn-secondary">
                                                    <input autocomplete="off" id="option5" name="team" type="radio" value="No">
                                                        No
                                                
                                                </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <legend class="col-form-label pt-0">
                                                Will you need product design support (UX/UI)?
                                            </legend>
                                            <div>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary">
                                                    <input autocomplete="off" checked="" id="option1" name="support" type="radio" value="Yes">
                                                        Yes
                                                </label>
                                                    <label class="btn btn-secondary active">
                                                    <input autocomplete="off" id="option2" name="support" type="radio" value="No">
                                                        No
                                                </label>
                                                    <label class="btn btn-secondary">
                                                    <input autocomplete="off" id="option3" name="support" type="radio" value="I don't Know">
                                                        I don't know
                                                
                                                </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <input type="hidden" class="monthly_cost" name="monthly_cost">
                                        <input type="hidden" class="members" name="members">
                                        <div class="text-right">
                                            <a href="#" class="submitForm btn btn-primary">Submit</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 right-panel">
                                        <div class="cost-area mb-4">
                                            <h5 class="text-muted text-center">
                                                Monthly Cost
                                            </h5>
                                            <div class="stored-cost">
                                            </div>
                                        </div>
                                        <p class="select-stmnt">
                                            Select your team members to continue...
                                        </p>
                                        <div class="selected-team-area">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1>
                            Time To Meet
                        </h1>
                        <div class="step-content">
                            <div class="text-center m-t-md">
                                <!-- Calendly inline widget begin -->
                                <div class="calendly-inline-widget" data-url="https://calendly.com/sabry/30min" style="min-width:320px;height:580px;">
                                </div>
                                <script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript">
                                </script>
                                <!-- Calendly inline widget end -->
                            </div>
                        </div>
                        <h1>
                            Complete Your Request
                        </h1>
                        <div class="step-content">
                            <div class="text-center m-t-md">
                                <div class="mt-5">
                                    <div class="row">
                                        <div class="modalbox success col-sm-8 col-md-6 col-lg-5 center animate">
                                            <div class="icon">
                                                <span class="fas fa-check">
                                            </span>
                                            </div>
                                            <!--/.icon-->
                                            <h1>
                                                Success!
                                            </h1>
                                            <p>
                                                We've sent a confirmation to your e-mail
                                                <br> for verification.

                                            </p>
                                        </div>
                                        <!--/.success-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
            </div>
        </div>
    </div>
    <!--Popper for bootstrap dropdown menus (must be included before bootstrap JS)-->
    <script src="js/popper.min.js">
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
    <!-- Steps -->
    <script src="js/steps/jquery.steps.min.js">
    </script>
    <script>
        $(document).ready(function() {

            $('[data-target=#bookYourTeam]').click(function() {

                $("#wizard-t-0").click();
                $("[aria-selected='false']").removeClass('done');
                $('[aria-selected="false"]').addClass('disabled');
                $("#wizard .actions a[href='#previous']").show();
                $("#wizard .actions a[href='#cancel']").show();
            });

            $("#wizard").steps({
                startIndex: 0,
                saveState: false,
                enableCancelButton: true,
                transitionEffect: "fade",
                labels: {
                    cancel: "Cancel",
                    current: "current step:",
                    pagination: "Pagination",
                    finish: "Finish",
                    next: "Continue",
                    previous: "Previous",
                    loading: "Loading ..."
                },
                onCanceled: function() {
                    $("#bookYourTeam").modal('hide');
                },
                onStepChanging: function(event, currentIndex, newIndex) {

                    if (newIndex == 1) {
                        $("#wizard .actions a[href='#next']").hide();
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }

                    return true;
                },
                onStepChanged: function(event, currentIndex, priorIndex) {
                    // console.log({event, currentIndex, priorIndex})
                    if (currentIndex == 3) {
                        $("#wizard .actions a[href='#previous']").hide();
                        $("#wizard .actions a[href='#cancel']").hide();
                    }
                },
                onFinished: function() {
                    $("#bookYourTeam").modal('hide');
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $(".submitForm").click(function() {
                var url = "ajax.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#wizard").serialize(),
                    success: function(res) {
                        $("#sendSuccess").text(res);
                        $("#sendSuccess").slideDown();
                        setTimeout(function() {
                            $("#sendSuccess").slideUp();
                        }, 3000);
                        $("#wizard .actions a[href='#next']").click();
                        $("#wizard .actions a[href='#previous']").show();
                        $("#wizard .actions a[href='#cancel']").show();
                        $("#wizard .actions a[href='#next']").show();
                    },
                    error: function(req, status, err) {
                        console.log('something went wrong', status, err);
                    }
                });
            });
        });
    </script>


    <!-- Checkbox -->
    <script>
        var selectedTeam = [],
            cost = [],
            totalCost = 0,

            stmntText = function() {
                if (selectedTeam.length == 0) {
                    $(".select-stmnt").text("Select your team members to continue...");
                    $(".cost-area").css('display', 'none');

                } else {
                    $(".select-stmnt").html("You currently have <strong>" + selectedTeam.length + '</strong> member(s) in your team');
                    $(".cost-area").css('display', 'block');
                }
            };

        $(document).ready(function() {

            $(".custom-control-label").on('click', function() {

                if ($(this).siblings()[0].checked) {
                    totalCost = 0;

                    $(".selected-team-area").html('');
                    var index = selectedTeam.indexOf($(this).siblings()[0].id);
                    var costIndex = cost.indexOf($(this).siblings().attr('data-cost'));
                    selectedTeam.splice(index, 1);
                    cost.splice(costIndex, 1);
                    stmntText();

                    $(".cost-area-inner").html(function() {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;
                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {
                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);

                } else {

                    $(".selected-team-area").html('');
                    totalCost = 0;
                    selectedTeam.push($(this).siblings()[0].id);
                    cost.push($(this).siblings().attr('data-cost'));
                    stmntText();

                    $(".cost-area-inner").html(function() {
                        for (var i = 0; i < cost.length; i++) {
                            totalCost += parseInt(cost[i]);
                        }
                        storedCost = totalCost;

                        return totalCost
                    });

                    for (var i = 0; i < selectedTeam.length; i++) {

                        $(".selected-team-area").append('<div>' + selectedTeam[i] + '</div>');
                    }

                    $('.stored-cost').html(totalCost);
                        $(".monthly_cost").val(totalCost)
                        $(".members").val(selectedTeam)
                }

            });

        });
    </script>

</body>

</html>